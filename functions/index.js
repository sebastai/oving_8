const functions = require('firebase-functions');
const express = require("express");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

let app = express();
const main = express();
main.use('/api/v1', app);
// The listen promise can be used to wait for the web server to start (for instance in your tests)
let appListen = new Promise((resolve, reject) => {
    app.listen(3000, (error) => {
        if (error) return reject(error.message);

        console.log('Express server started');
        resolve();
    });
});

app.get("/", (req, res ) => {
    res.send({status: "Online"})
});

module.exports = {appListen};